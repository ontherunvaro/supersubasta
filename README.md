# superSubasta - a FIPA English Auction implementation #

Programa en Java/JADE basado en agentes que implementa una subasta inglesa.

This program is an implementation of the FIPA English Auction protocol using JADE agents and ontologies.

*Computación Distribuída (USC) Abril 2015*
Todos los derechos reservados