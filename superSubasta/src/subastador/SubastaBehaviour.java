package subastador;

import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import ontologia.AnswersBid;
import ontologia.AnswersBidOffer;
import ontologia.Libro;
import ontologia.OffersBid;
import ontologia.RequestsPayment;
import ontologia.Wins;

@SuppressWarnings("serial")
public class SubastaBehaviour extends TickerBehaviour {

	/**
	 * Incremento de precio entre dos rondas
	 */

	private float incremento = 5;

	private DFAgentDescription busqueda;

	private UUID identificador;

	private Libro libro;

	private MessageTemplate mt;

	private AID prevWinner;

	private List<AID> pujadores = new ArrayList<>();

	public SubastaBehaviour(Agent a, int milliseconds, Libro libro, Float incremento) {
		super(a, milliseconds);
		this.libro = libro;
		this.incremento=incremento;

		identificador = UUID.randomUUID();

		/* Inicializaci�n de la plantilla de b�squeda */
		DFAgentDescription temp = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("pujador");
		temp.addServices(sd);
		busqueda = temp;
		/**/
	}

	public SubastaBehaviour(Agent a, int milliseconds, String titulo, Float precio) {
		super(a, milliseconds);

		this.libro = new Libro(titulo, precio);

		identificador = UUID.randomUUID();

		/* Inicializaci�n de la plantilla de b�squeda */
		DFAgentDescription temp = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("pujador");
		temp.addServices(sd);
		busqueda = temp;
		/**/
	}

	public UUID getIdentificador() {
		return identificador;
	}

	@Override
	public void onStart() {
		System.out.println("Subastando " + libro.getTitulo() + " con precio inicial=" + libro.getPrecio() + "�");
	}

	@Override
	public void onTick() {

		try {
			/* Obtener lista de compradores */
			pujadores.clear();
			DFAgentDescription[] result = DFService.search(myAgent, busqueda);
			for (DFAgentDescription d : result) {
				pujadores.add(d.getName());
			}

			Collections.shuffle(pujadores); // no favorecer siempre al primero
			/*******************************/

			if (!pujadores.isEmpty()) {// si no hay pujadores, esperar al
										// siguiente ciclo

				Ontology onto = ((AgenteSubastador) myAgent).ontology;
				Codec codec = ((AgenteSubastador) myAgent).codec;

				/* Preparar y enviar CFP */
				OffersBid of = new OffersBid(libro);

				ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
				cfp.setOntology(onto.getName());
				cfp.setLanguage(codec.getName());
				try {
					myAgent.getContentManager().fillContent(cfp, new Action(myAgent.getAID(), of));
				} catch (CodecException | OntologyException e) {
					e.printStackTrace();
				}
				cfp.setSender(myAgent.getAID());
				cfp.setConversationId("subasta:" + identificador);
				cfp.setReplyWith("cfp" + System.currentTimeMillis());

				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("subasta:" + identificador), MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
				mt = MessageTemplate.and(mt, MessageTemplate.MatchOntology(onto.getName()));

				for (AID p : pujadores) {
					cfp.addReceiver(p);
				}
				myAgent.send(cfp);
				/**************************/

				/* Recibir PROPOSAL de los pujadores */
				int reply_count = 0; // cuenta de respuestas
				int interested_count = 0; // cuenta de interesados
				boolean have_winner = false; // tenemos ya ganador de esta
												// ronda?

				do {

					ACLMessage rcv = myAgent.receive(mt);
					if (rcv != null) {
						Action ac = null;

						try {
							ac = (Action) myAgent.getContentManager().extractContent(rcv);
						} catch (CodecException | OntologyException e) {
							e.printStackTrace();
						}

						AnswersBidOffer answersBidOffer = (AnswersBidOffer) ac.getAction();

						if (answersBidOffer.getAccepted()) {
							if (!have_winner) {
								have_winner = true;
								prevWinner = ac.getActor();
							}
							interested_count++;
						}
						reply_count++;
					} else {
						block();
					}

				} while (reply_count < pujadores.size());
				/*************************************/

				/* Responder a los PROPOSAL */
				AnswersBid ab = new AnswersBid(libro, true);
				ACLMessage answerP = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
				answerP.addReceiver(prevWinner);
				answerP.setSender(myAgent.getAID());
				answerP.setOntology(onto.getName());
				answerP.setLanguage(codec.getName());
				try {
					myAgent.getContentManager().fillContent(answerP, new Action(myAgent.getAID(), ab));
				} catch (CodecException | OntologyException e) {
					e.printStackTrace();
				}
				answerP.setConversationId("subasta:" + identificador);
				myAgent.send(answerP);

				ab = new AnswersBid(libro, false);
				answerP = new ACLMessage(ACLMessage.REJECT_PROPOSAL);
				answerP.setSender(myAgent.getAID());
				answerP.setOntology(onto.getName());
				answerP.setLanguage(codec.getName());
				try {
					myAgent.getContentManager().fillContent(answerP, new Action(myAgent.getAID(), ab));
				} catch (CodecException | OntologyException e) {
					e.printStackTrace();
				}

				answerP.setConversationId("subasta:" + identificador);
				for (AID p : pujadores) {
					if (!p.equals(prevWinner)) {
						answerP.addReceiver(p);
					}

				}
				myAgent.send(answerP);

				/****************************/

				/* Resolucion de la ronda */
				boolean ended = false;
				switch (interested_count) {
				case 0:
					if (prevWinner != null) {
						System.out.println(prevWinner.getLocalName() + " gana la subasta por " + libro.getPrecio() + " �");
						ended = true;
					}
					// Si prevWinner==null, estamos en la 1a ronda y no hay
					// interesados -> se repite la ronda
					break;

				case 1:
					System.out.println(prevWinner.getLocalName() + " gana la subasta por " + libro.getPrecio() + " �");
					ended = true;
					break;

				default:
					// si no hay ganador, se pasa a la siguiente ronda
					libro.setPrecio(libro.getPrecio() + incremento);
					((AgenteSubastador) myAgent).actualizarSubasta(identificador, libro.getPrecio());
					break;

				}
				/****************************/

				/* Informar a los participantes y terminar la subasta */
				if (ended) {
					Wins wins = new Wins();
					wins.setLibro(libro);

					ACLMessage informar = new ACLMessage(ACLMessage.INFORM);
					informar.setSender(myAgent.getAID());
					informar.setConversationId("subasta:" + identificador);
					informar.setOntology(onto.getName());
					informar.setLanguage(codec.getName());
					try {
						myAgent.getContentManager().fillContent(informar, new Action(prevWinner, wins));
					} catch (CodecException | OntologyException e) {
						e.printStackTrace();
					}
					for (AID p : pujadores) {
						if (!p.equals(prevWinner))
							informar.addReceiver(p);
					}
					myAgent.send(informar);

					RequestsPayment rp = new RequestsPayment(libro);
					ACLMessage req = new ACLMessage(ACLMessage.REQUEST);
					req.setSender(myAgent.getAID());
					req.setConversationId("subasta:" + identificador);
					req.setOntology(onto.getName());
					req.setLanguage(codec.getName());
					try {
						myAgent.getContentManager().fillContent(req, new Action(myAgent.getAID(), rp));
					} catch (CodecException | OntologyException e) {
						e.printStackTrace();
					}
					req.addReceiver(prevWinner);
					((AgenteSubastador) myAgent).finalizarSubasta(identificador, prevWinner.getLocalName());
					myAgent.send(req);

					stop();

				}
				/*******************************/

			}

		} catch (FIPAException fe) {
			fe.printStackTrace();
		}

	}

	public void setIdentificador(UUID identificador) {
		this.identificador = identificador;
	}

}
