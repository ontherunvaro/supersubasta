package subastador.gui;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TablaSubastas extends AbstractTableModel {

	private class Subasta {
		public String ganador;
		public Float precio;
		public String titulo;

		public Subasta(String titulo, Float precio) {
			this.titulo = titulo;
			this.precio = precio;
			this.ganador = "En curso";
		}
	}

	private Map<UUID, Subasta> subastas;

	public TablaSubastas() {
		subastas = new HashMap<>();
	}

	public void actualizarSubasta(UUID a, float precio2) {
		subastas.get(a).precio = precio2;

		fireTableDataChanged();
	}

	public void addSubasta(UUID id, String titulo, Float precio) {
		this.subastas.put(id, new Subasta(titulo, precio));
		fireTableDataChanged();
	}

	public void finalizarSubasta(UUID identificador, String ganador) {
		subastas.get(identificador).ganador = ganador;
		fireTableDataChanged();
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
		case 0:
			return "Libro";
		case 1:
			return "Precio actual";
		case 2:
			return "Ganador";
		}
		return "default";
	}

	@Override
	public int getRowCount() {
		return this.subastas.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return subastas.get(subastas.keySet().toArray()[rowIndex]).titulo;
		case 1:
			return subastas.get(subastas.keySet().toArray()[rowIndex]).precio.toString() + "�";
		case 2:
			return subastas.get(subastas.keySet().toArray()[rowIndex]).ganador;
		}
		return "default";
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

}
