package subastador.gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import subastador.AgenteSubastador;

@SuppressWarnings("serial")
public class SubastadorGui extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					SubastadorGui frame = new SubastadorGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private AgenteSubastador ag;
	private final JButton btnA�adir = new JButton("A\u00F1adir");
	private JPanel contentPane;
	private final JLabel lblPrecio = new JLabel("Precio inicial");
	private final JLabel lblSubastas = new JLabel("Subastas");
	private final JLabel lblTitulo = new JLabel("Titulo");
	private final JTextField precio = new JTextField();
	private final JScrollPane scrollPane = new JScrollPane();
	private final JTable table = new JTable();

	private final JTextField titulo = new JTextField();
	private final JTextField incremento = new JTextField();
	private final JLabel lblIncremento = new JLabel("Incremento");

	/**
	 * Create the frame.
	 */
	public SubastadorGui() {
		this.incremento.setBounds(296, 36, 43, 20);
		this.incremento.setColumns(10);

		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		setTitle("Subastador");
		setResizable(false);
		this.precio.setBounds(296, 8, 43, 20);
		this.precio.setColumns(10);
		this.titulo.setBounds(63, 8, 130, 20);
		this.titulo.setColumns(10);
		setBounds(100, 100, 444, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.lblTitulo.setHorizontalAlignment(SwingConstants.RIGHT);
		this.lblTitulo.setBounds(10, 11, 43, 14);

		contentPane.add(this.lblTitulo);

		contentPane.add(this.titulo);
		this.lblPrecio.setHorizontalAlignment(SwingConstants.RIGHT);
		this.lblPrecio.setBounds(211, 11, 75, 14);

		contentPane.add(this.lblPrecio);

		contentPane.add(this.precio);
		this.btnA�adir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_btnA�adir_actionPerformed(e);
			}
		});
		this.btnA�adir.setBounds(345, 7, 89, 23);

		contentPane.add(this.btnA�adir);
		this.lblSubastas.setFont(new Font("Tahoma", Font.BOLD, 11));
		this.lblSubastas.setHorizontalAlignment(SwingConstants.CENTER);
		this.lblSubastas.setBounds(10, 62, 424, 14);

		contentPane.add(this.lblSubastas);
		this.scrollPane.setBounds(10, 87, 418, 173);

		contentPane.add(this.scrollPane);
		this.scrollPane.setViewportView(this.table);
		this.table.setModel(new TablaSubastas());
		
		contentPane.add(this.incremento);
		this.lblIncremento.setHorizontalAlignment(SwingConstants.RIGHT);
		this.lblIncremento.setBounds(211, 37, 75, 14);
		
		contentPane.add(this.lblIncremento);
	}

	public SubastadorGui(AgenteSubastador ag) {
		this();
		this.ag = ag;
	}

	public void actualizarSubasta(UUID a, float precio2) {
		TablaSubastas t = (TablaSubastas) this.table.getModel();
		t.actualizarSubasta(a, precio2);
	}

	public void addSubasta(UUID a, String titulo2, float precio2) {
		TablaSubastas t = (TablaSubastas) this.table.getModel();
		t.addSubasta(a, titulo2, precio2);
	}

	protected void do_btnA�adir_actionPerformed(ActionEvent e) {
		ag.a�adirLibro(this.titulo.getText().trim(), Float.parseFloat(this.precio.getText().trim()), Float.parseFloat((this.incremento.getText().trim())));
	}

	public void finalizarSubasta(UUID identificador, String ganador) {
		TablaSubastas t = (TablaSubastas) this.table.getModel();
		t.finalizarSubasta(identificador, ganador);

	}
}
