package subastador;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import java.util.ArrayList;
import java.util.UUID;

import ontologia.Libro;
import ontologia.OntologiaSubasta;
import subastador.gui.SubastadorGui;

@SuppressWarnings("serial")
public class AgenteSubastador extends Agent {

	Codec codec = new SLCodec();
	private SubastadorGui gui;
	Ontology ontology = OntologiaSubasta.getInstance();

	public void actualizarSubasta(UUID a, float precio) {
		gui.actualizarSubasta(a, precio);
	}

	public void a�adirLibro(String titulo, float precio, float incremento) {
		Libro l=new Libro(titulo,precio);
		SubastaBehaviour k = new SubastaBehaviour(this, 10000, l, incremento);
		addBehaviour(k);
		gui.addSubasta(k.getIdentificador(), titulo, precio);
	}

	public void finalizarSubasta(UUID identificador, String ganador) {
		gui.finalizarSubasta(identificador, ganador);
	}

	@Override
	protected void setup() {
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("subastador");
		sd.setName(getName());
		sd.setOwnership("ComDis");
		dfd.setName(getAID());
		dfd.addServices(sd);

		Object[] args = getArguments();
		ArrayList<Libro> libros = new ArrayList<>();

		if (args != null && (args.length % 3) == 0) {

			gui = new SubastadorGui(this);

			/* Registrar ontologia */
			getContentManager().registerLanguage(codec);
			getContentManager().registerOntology(ontology);
			/*********************/

			try {
				DFService.register(this, dfd);

				for (int i = 0; i < args.length; i += 3) {

					String lt = (String) args[i];
					Float pt = Float.parseFloat((String) args[i + 1]);
					Float inc= Float.parseFloat((String) args[i+2]);
					Libro l = new Libro(lt, pt);
					libros.add(l);
					SubastaBehaviour a = new SubastaBehaviour(this, 10000, l,inc);
					addBehaviour(a);
					gui.addSubasta(a.getIdentificador(), l.getTitulo(), l.getPrecio());
				}

				gui.setVisible(true);
			} catch (FIPAException e) {
				System.out.println(e.getMessage());
				doDelete();
			}

		} else {
			System.out.println(getLocalName() + ": argumentos inv�lidos");
			doDelete();
		}

	}

	@Override
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (Exception e) {
		}
		super.takeDown();
	}
}
