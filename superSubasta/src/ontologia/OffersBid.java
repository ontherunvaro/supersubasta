package ontologia;

import jade.content.AgentAction;

@SuppressWarnings("serial")
public class OffersBid implements AgentAction {

	private Libro libro;

	public OffersBid() {
	}

	public OffersBid(Libro libro) {
		this.libro = libro;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

}
