package ontologia;

import jade.content.Concept;

@SuppressWarnings("serial")
public class Libro implements Concept {

	private Float precio;
	private String titulo;

	public Libro() {
	}

	public Libro(String titulo) {
		this.titulo = titulo;
	}

	public Libro(String titulo, Float precio) {
		this.titulo = titulo;
		this.precio = precio;
	}

	public Float getPrecio() {
		return precio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
