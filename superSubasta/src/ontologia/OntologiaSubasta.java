package ontologia;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PrimitiveSchema;

@SuppressWarnings("serial")
public class OntologiaSubasta extends Ontology {

	public final static String ACCEPTS = "accepted";

	public final static String ANSWERS_BID = "AnswersBid";
	public final static String ANSWERS_BID_OFFER = "AnswersBidOffer";
	public final static String LIBRO = "Libro";

	public final static String OFFERS_BID = "OffersBid";
	public final static String ONTOLOGY_NAME = "Ontologia-subasta";
	public final static String PRECIO = "precio";
	public final static String REQUESTS_PAYMENT = "RequestsPayment";

	private static Ontology singleton = new OntologiaSubasta();

	public final static String TITULO = "titulo";

	public final static String WINNER = "winner";

	public final static String WINS = "Wins";

	public static Ontology getInstance() {
		return singleton;
	}

	private OntologiaSubasta() {
		super(ONTOLOGY_NAME, BasicOntology.getInstance());

		try {

			add(new ConceptSchema(LIBRO), Libro.class);
			ConceptSchema cs = (ConceptSchema) getSchema(LIBRO);
			cs.add(TITULO, (PrimitiveSchema) getSchema(BasicOntology.STRING));
			cs.add(PRECIO, (PrimitiveSchema) getSchema(BasicOntology.FLOAT), ObjectSchema.OPTIONAL); // el
																										// libro
																										// puede
																										// no
																										// tener
																										// precio

			add(new AgentActionSchema(OFFERS_BID), OffersBid.class);
			AgentActionSchema aas = (AgentActionSchema) getSchema(OFFERS_BID);
			aas.add(LIBRO, (ConceptSchema) getSchema(LIBRO));

			add(new AgentActionSchema(ANSWERS_BID_OFFER), AnswersBidOffer.class);
			aas = (AgentActionSchema) getSchema(ANSWERS_BID_OFFER);
			aas.add(LIBRO, (ConceptSchema) getSchema(LIBRO));
			aas.add(ACCEPTS, (PrimitiveSchema) getSchema(BasicOntology.BOOLEAN));

			add(new AgentActionSchema(ANSWERS_BID), AnswersBid.class);
			aas = (AgentActionSchema) getSchema(ANSWERS_BID);
			aas.add(ACCEPTS, (PrimitiveSchema) getSchema(BasicOntology.BOOLEAN));
			aas.add(LIBRO, (ConceptSchema) getSchema(LIBRO));

			add(new AgentActionSchema(WINS), Wins.class);
			aas = (AgentActionSchema) getSchema(WINS);
			aas.add(LIBRO, (ConceptSchema) getSchema(LIBRO));

			add(new AgentActionSchema(REQUESTS_PAYMENT), RequestsPayment.class);
			aas = (AgentActionSchema) getSchema(REQUESTS_PAYMENT);
			aas.add(LIBRO, (ConceptSchema) getSchema(LIBRO));

		} catch (OntologyException oe) {
			oe.printStackTrace();
		}

	}

}
