package ontologia;

import jade.content.AgentAction;

@SuppressWarnings("serial")
public class AnswersBidOffer implements AgentAction {

	private boolean accepted;
	private Libro libro;

	public AnswersBidOffer() {
	}

	public AnswersBidOffer(Libro libro, boolean accepts) {
		this.libro = libro;
		this.accepted = accepts;
	}

	public boolean getAccepted() {
		return accepted;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setAccepted(boolean accepts) {
		this.accepted = accepts;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

}
