package ontologia;

import jade.content.AgentAction;

@SuppressWarnings("serial")
public class RequestsPayment implements AgentAction {

	private Libro libro;

	public RequestsPayment() {
	}

	public RequestsPayment(Libro libro) {
		this.libro = libro;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}
}
