package ontologia;

import jade.content.AgentAction;

@SuppressWarnings("serial")
public class AnswersBid implements AgentAction {

	private boolean accepted;
	private Libro libro;

	public AnswersBid() {
	}

	public AnswersBid(Libro libro, boolean accepted) {
		super();
		this.libro = libro;
		this.accepted = accepted;
	}

	public boolean getAccepted() {
		return accepted;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

}
