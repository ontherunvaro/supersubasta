package pujador;

import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.HashMap;
import java.util.Map;

import ontologia.AnswersBidOffer;
import ontologia.Libro;
import ontologia.OffersBid;
import ontologia.OntologiaSubasta;
import ontologia.RequestsPayment;
import ontologia.Wins;
import pujador.gui.PujadorGui;

@SuppressWarnings("serial")
public class AgentePujador extends Agent {

	private class ListenForEnd extends CyclicBehaviour {

		public ListenForEnd(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
			mt = MessageTemplate.and(mt, MessageTemplate.MatchOntology(onto.getName()));
			mt = MessageTemplate.and(mt, MessageTemplate.MatchLanguage(codec.getName()));
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				Wins wins = null;
				Action a = null;
				try {
					a = (Action) getContentManager().extractContent(msg);
					wins = (Wins) a.getAction();
				} catch (CodecException | OntologyException e) {
					e.printStackTrace();
				}

				String lib = wins.getLibro().getTitulo();
				if (libros.containsKey(lib) && !a.getActor().equals(myAgent.getAID()))
					gui.cambiarEstado(lib, "Esperando subasta");

			} else {
				mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
				msg = myAgent.receive(mt);

				if (msg != null) {
					RequestsPayment rp = null;
					Action a = null;
					try {
						a = (Action) getContentManager().extractContent(msg);
						rp = (RequestsPayment) a.getAction();
					} catch (CodecException | OntologyException e) {
						e.printStackTrace();
					}
					Libro lib = rp.getLibro();
					if (libros.containsKey(lib.getTitulo())) {
						libros.remove(lib.getTitulo());
						gui.cambiarEstado(lib.getTitulo(), "Comprado por " + lib.getPrecio() + "�");
						if (libros.isEmpty())
							myAgent.doDelete();
						;
					}

				} else {
					block();
				}
			}

		}
	}
	private class PujaBehaviour extends CyclicBehaviour {

		public PujaBehaviour(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.CFP), MessageTemplate.MatchOntology(onto.getName()));

			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				/*
				 * String[] cont = msg.getContent().split(":"); String title =
				 * cont[0]; Float precio = Float.parseFloat(cont[1]);
				 */

				Action ac = null;
				try {
					ac = (Action) getContentManager().extractContent(msg);
				} catch (CodecException | OntologyException e) {
					e.printStackTrace();
				}
				OffersBid ob = (OffersBid) ac.getAction();
				String title = ob.getLibro().getTitulo();
				Float precio = ob.getLibro().getPrecio();

				System.out.println(getLocalName() + " recibe puja: " + title + ", " + precio + "�");

				ACLMessage reply = msg.createReply();
				reply.setSender(myAgent.getAID());
				reply.setPerformative(ACLMessage.PROPOSE);
				reply.setOntology(onto.getName());

				AnswersBidOffer aw = new AnswersBidOffer();
				aw.setLibro(ob.getLibro());

				if (precio != null && title != null && precio > 0) {

					if (libros.containsKey(title) && precio <= libros.get(title)) {
						aw.setAccepted(true);
						System.out.println(getLocalName() + " acepta pujar");
						gui.cambiarEstado(title, "En subasta. �ltima puja: " + precio + "�");
					}

					else {
						aw.setAccepted(false);
						System.out.println(getLocalName() + " rechaza pujar");
						if (libros.containsKey(title)) {
							gui.cambiarEstado(title, "Subasta ha superado puja m�xima.");
						}
					}

					try {
						myAgent.getContentManager().fillContent(reply, new Action(myAgent.getAID(), aw));
					} catch (CodecException | OntologyException e) {
						e.printStackTrace();
					}

				} else {
					reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
					reply.setContent("Mensaje invalido (precio no valido)");
				}
				myAgent.send(reply);
			} else {
				block();
			}
		}

	}
	private Codec codec;
	private PujadorGui gui;

	private Map<String, Float> libros;

	private Ontology onto;

	@Override
	protected void setup() {
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("pujador");
		sd.setName(getName());
		sd.setOwnership("ComDis");
		dfd.setName(getAID());
		dfd.addServices(sd);

		Object[] args = getArguments();
		libros = new HashMap<>();

		if (args != null && args.length >= 2 && (args.length % 2) == 0) {

			for (int i = 0; i < args.length; i += 2) {
				String lt = (String) args[i];
				Float pt = Float.parseFloat((String) args[i + 1]);
				libros.put(lt, pt);
			}

			String info = getLocalName() + " pujando por: ";

			for (String k : libros.keySet())
				info = info + "\n\t" + k + " : " + libros.get(k) + "�";

			System.out.println(info);

			gui = new PujadorGui(this.getLocalName(),this);
			gui.setLibros(libros);
			gui.setVisible(true);

			try {
				DFService.register(this, dfd);
				PujaBehaviour comp = new PujaBehaviour(this);
				addBehaviour(comp);
				addBehaviour(new ListenForEnd(this));
			} catch (FIPAException e) {
				System.out.println(e.getMessage());
				doDelete();
			}

			/* Registrar ontologia */
			codec = new SLCodec();
			onto = OntologiaSubasta.getInstance();
			getContentManager().registerLanguage(codec);
			getContentManager().registerOntology(onto);
			/*********************/

		} else {
			System.out.println(getLocalName() + ": argumentos invalidos");
			doDelete();
		}

	}

	@Override
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (Exception e) {
		}
		super.takeDown();
	}

	public void addLibro(Libro libro) {
		this.libros.put(libro.getTitulo(), libro.getPrecio());		
	}

}
