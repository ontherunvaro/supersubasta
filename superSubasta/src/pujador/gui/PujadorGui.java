package pujador.gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ontologia.Libro;
import pujador.AgentePujador;

@SuppressWarnings("serial")
public class PujadorGui extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					PujadorGui frame = new PujadorGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private JPanel contentPane;
	private final JLabel labelTitulo = new JLabel("Pujador - ");
	private final JScrollPane scrollPane = new JScrollPane();

	private final JTable table = new JTable();
	private final JTextField titulo = new JTextField();
	private final JTextField precio = new JTextField();
	private final JButton btnAdd = new JButton("A\u00F1adir");
	private final JLabel lblPrecioMaximo = new JLabel("Precio maximo");
	private final JLabel lblTitulo = new JLabel("Titulo");
	
	private AgentePujador ag;

	/**
	 * Create the frame.
	 */
	public PujadorGui() {
		this.precio.setBounds(278, 37, 59, 20);
		this.precio.setColumns(10);
		this.titulo.setBounds(77, 37, 125, 20);
		this.titulo.setColumns(10);
		setTitle("Pujador");
		setResizable(false);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.labelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		this.labelTitulo.setFont(new Font("Tahoma", Font.BOLD, 11));
		this.labelTitulo.setBounds(10, 10, 424, 23);

		contentPane.add(this.labelTitulo);
		this.scrollPane.setBounds(10, 68, 424, 192);

		contentPane.add(this.scrollPane);

		this.table.setModel(new TablaPujador());

		this.scrollPane.setViewportView(this.table);
		
		contentPane.add(this.titulo);
		
		contentPane.add(this.precio);
		this.btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddactionPerformed(e);
			}
		});
		this.btnAdd.setBounds(345, 34, 89, 23);
		
		contentPane.add(this.btnAdd);
		this.lblPrecioMaximo.setToolTipText("Precio maximo");
		this.lblPrecioMaximo.setBounds(222, 40, 46, 14);
		
		contentPane.add(this.lblPrecioMaximo);
		this.lblTitulo.setBounds(20, 40, 46, 14);
		
		contentPane.add(this.lblTitulo);
	}

	public PujadorGui(String nombrePujador, AgentePujador a) {
		this();
		this.labelTitulo.setText("Pujador - " + nombrePujador);
		this.ag=a;
	}

	public void cambiarEstado(String title, String string) {
		TablaPujador j = (TablaPujador) this.table.getModel();
		j.cambiarEstado(title, string);
	}

	public void setLibros(Map<String, Float> libros) {
		TablaPujador j = (TablaPujador) this.table.getModel();
		j.setLibros(libros);
	}
	protected void btnAddactionPerformed(ActionEvent e) {
		Libro l=new Libro(titulo.getText().trim(),Float.parseFloat(precio.getText().trim()));
		ag.addLibro(l);
		TablaPujador j = (TablaPujador) this.table.getModel();
		j.addLibro(l.getTitulo(),l.getPrecio());
	}
}
