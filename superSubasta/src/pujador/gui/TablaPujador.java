package pujador.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TablaPujador extends AbstractTableModel {

	private class Libro {
		public String estado;
		public Float precioM;
		public String titulo;

		public Libro(String titulo, Float precioM) {
			this.titulo = titulo;
			this.precioM = precioM;
			this.estado = "Esperando subasta";
		}
	}

	private List<Libro> libros;

	public TablaPujador() {
		libros = new ArrayList<>();
	}

	public void cambiarEstado(String titulo, String estado) {
		Libro k = null;
		for (Libro a : libros)
			if (a.titulo.equals(titulo)) {
				k = a;
				break;
			}
		k.estado = estado;
		fireTableDataChanged();

	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
		case 0:
			return "Libro";
		case 1:
			return "Precio m�ximo";
		case 2:
			return "Estado";
		}
		return "default";
	}

	@Override
	public int getRowCount() {
		return libros.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return libros.get(rowIndex).titulo;
		case 1:
			return libros.get(rowIndex).precioM;
		case 2:
			return libros.get(rowIndex).estado;
		}
		return "default";
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public void setLibros(Map<String, Float> libros) {
		for (String k : libros.keySet()) {
			this.libros.add(new Libro(k, libros.get(k)));
		}
		fireTableDataChanged();
	}

	public void addLibro(String titulo, Float precio) {
		Libro k=new Libro(titulo,precio);
		this.libros.add(k);
		fireTableDataChanged();
	}

}
